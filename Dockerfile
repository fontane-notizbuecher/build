# Debian (ant, git)
#
# VERSION               0.0.3

FROM frekele/ant

LABEL Description="build env for eXist-db + applications supporting various tests" Version="1.0"

RUN apt-get update && apt-get upgrade -yy && apt-get install -y git graphviz jq ant-contrib bzip2
