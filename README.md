# Fontane Build Scripts
[![pipeline status](https://gitlab.gwdg.de/fontane-notizbuecher/build/badges/develop/pipeline.svg)](https://gitlab.gwdg.de/fontane-notizbuecher/build/commits/develop)

all build scripts to prepare a complete database instance
## build
```bash
ant -f [BRANCH-NAME].xml
```

## usage
```bash
./build/sade/bin/startup.sh
```
at the first startup the database will be set up. therefore all packages from `build/sade/autodeploy` will be installed. mostly this will be done quietly, but there is a logfile at `build/sade/webapp/WEB-INF/logs/expath-repo.log`.

## development / CI and release
there are nightly builds available. these development releases are deployed to the staging server. They are created with all files matching the pattern `*-develop.*`.
When a release is made following the git flow procedure, the artifact from created from the master branch is deployed on the production server.

Artifacts are packed in Debian packages and published via the [DARIAH-DE Aptly](https://ci.de.dariah.eu/packages/) repository.


## update process in the project

To perform an update on productiv env the following steps listed here are
required.

### version numbers
Increase a minor version, when notebooks are going to be deployed. please keep
the version numbers in synch - except there are no changes done to a package,
e.g. textgrid-sade-connect that receives updates only occasionally.

### hotfixes
To release a hotfix simply prepare a hotfix release in the repository and rerun
the latest master pipeline in the this repo. Or – if the version number should
be increased – prepare a complete hotfix release here.

### steps to update
1. update master of fontane-notizbuecher/SADE
   * git flow release start "VERSION"
1. update master of fontane-notizbuecher/SADE-Project
   * git flow release start "VERSION"
1. have a coffee until both packages are deployed to the exist-repo
1. update this master
   * git flow release start "VERSION"
