#!/bin/bash
# these four curl requests trigger the publication of the named URIs in the db.
# they are sorted that fast running are first.

simpleTest () {
if [ $(curl -s -o /dev/null -w "%{http_code}" $2) == "200" ];
  then
    echo "$1: OK";
  else
    echo "$1: FAILED";
    "$(date) ::: error ::: $1 ::: $2" > error.log
    curl $2 | sed -e 's/<[^>]*>//g' >> error.log
    exit 1;
fi
}

simpleTestTwo () {
if [ $(curl -s -o /dev/null -w "%{http_code}" $2) == "200" ];
  then
    echo "$1: OK";
  else
    test2=$(curl -s -o /dev/null -w "%{http_code}" $2) == "200"
	if [[ $(curl -s -o /dev/null -w "%{http_code}" $2) == "200" ]];
	  then
	    echo "$1: OK";
	  else
	    echo "$1: FAILED"; echo ${test2};
      "$(date) ::: error ::: $1 ::: $2" > error.log
      curl $2 | sed -e 's/<[^>]*>//g' >> error.log
      exit 1;
	fi
fi
}

# basic tests that make the application fail
simpleTestTwo "basic" "http://localhost:8080/exist/apps/SADE/textgrid/index.html"

echo "starting import at $(date)"
curl --output trigger-publish1.xml --silent --retry 3 --retry-delay 5 --retry-all-errors "http://localhost:8080/exist/rest/db/apps/fontane/trigger-publish.xql?pass=&uris=textgrid:1zwx0,textgrid:1rhds,textgrid:22jtj,textgrid:22jtb,textgrid:1rhdr,textgrid:22jtx,textgrid:22jtf,textgrid:1rhdm,textgrid:22jtt,textgrid:1rhdp,textgrid:2128p,textgrid:1zzdz,textgrid:1rhdn,textgrid:2128s,textgrid:22jtv,textgrid:1zzdm" &
curl --output trigger-publish2.xml --silent --retry 3 --retry-delay 5 --retry-all-errors "http://localhost:8080/exist/rest/db/apps/fontane/trigger-publish.xql?pass=&uris=textgrid:22jth,textgrid:1zzdn,textgrid:22jtm,textgrid:22jts,textgrid:22jtg,textgrid:22jt7,textgrid:22jtc,textgrid:22jt9,textgrid:22jtn,textgrid:1zzf4,textgrid:1zzds,textgrid:1zzdp,textgrid:2128n,textgrid:22jtq,textgrid:2128h,textgrid:16bsn,textgrid:1zzdr" &
curl --output trigger-publish3.xml --silent --retry 3 --retry-delay 5 --retry-all-errors "http://localhost:8080/exist/rest/db/apps/fontane/trigger-publish.xql?pass=&uris=textgrid:22jtk,textgrid:1zzdq,textgrid:22jt8,textgrid:2128k,textgrid:1zzf3,textgrid:1rhdq,textgrid:1rhdh,textgrid:22jtr,textgrid:16q91,textgrid:2128d,textgrid:22jtp,textgrid:1zzf1,textgrid:2128q,textgrid:2128j" &
curl --output trigger-publish4.xml --silent --retry 3 --retry-delay 5 --retry-all-errors "http://localhost:8080/exist/rest/db/apps/fontane/trigger-publish.xql?pass=&uris=textgrid:16q90,textgrid:16b9z,textgrid:2128g,textgrid:169zx,textgrid:22jtd,textgrid:1zzdw,textgrid:2128t,textgrid:2128f,textgrid:1zzf2,textgrid:1zzf0,textgrid:16b00,textgrid:1zzdt,textgrid:1zzdk,textgrid:2128r,textgrid:1rhdj,textgrid:22jtw,textgrid:1zzdx,textgrid:2128m,textgrid:1zzdv,textgrid:1rhdk" &
wait
echo "done at $(date)."

# testing
NUM=$(grep 'report uri' trigger-publish*.xml | wc -l)
if [ ${NUM} -gt 0 ];
  then echo "non empty log found, there are ${NUM} reports available.";
  else echo "all logs are empty. publication failed."; exit 1;
fi

# print errors
grep -B 1 -A 1 "<error" *.xml
echo -e "\u001B[1;30;103;mThere are `grep "<error" *.xml | wc -l` errors reported.\u001B[0m"

# validation status
echo -e "\u001B[1;30;102;mThere are `grep "<status>valid</status>" *.xml | wc -l` valid documents.\u001B[0m"
echo -e "\u001B[1;97;101;mThere are `grep "<status>invalid</status>" *.xml | wc -l` invalid documents.\u001B[0m"
